#!/bin/bash

if [ ! -f /etc/nginx/ssl/dev.crt ]; then
    openssl genrsa -out "/etc/nginx/ssl/dev.key" 2048
    openssl req -new -key "/etc/nginx/ssl/dev.key" -out "/etc/nginx/ssl/dev.csr" -subj "/CN=${DC_DOMAIN_NAME}/O=LSG/C=DK"
    openssl x509 -req -days 365 -in "/etc/nginx/ssl/dev.csr" -signkey "/etc/nginx/ssl/dev.key" -out "/etc/nginx/ssl/dev.crt"
fi

# Start nginx in foreground
nginx -g "daemon off;"
